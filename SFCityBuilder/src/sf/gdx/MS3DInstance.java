package sf.gdx;

import sf.gdx.shaders.OverlayAttribute;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.*;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.model.*;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController;
import com.badlogic.gdx.utils.ObjectMap;

/** This probably needs a better name */
public class MS3DInstance {
	public ModelInstance mi;
	public AnimationController anim;
	
	public MS3DInstance(ModelInstance mi) {
		this.mi = mi;
		Node node = mi.nodes.get(0);
		for(NodePart np : node.parts) {
			partmap.put(np.meshPart.id, np);
		}
		anim = new AnimationController(mi);
		
		attr = new OverlayAttribute(null);
		for(Material mat : mi.materials) {
			mat.set(attr);
			//Attribute blending = mat.get(BlendingAttribute.Type);
			//System.out.println("Blending attribute: " + blending);
			//mat.set(new BlendingAttribute());
		}
	}
	
	public MS3DInstance(Model m) {
		this(new ModelInstance(m));
	}
	
	OverlayAttribute attr;
	
	public void setOverlaySkins(Texture...skins) {
		attr.textures = skins;
	}
	
	ObjectMap<String, NodePart> partmap = new ObjectMap<>();
	
    public void hidePart(String id) {
		partmap.get(id).enabled = false;
	}
    
    public void hideParts(String...ids) {
    	for(String id : ids) {
    		hidePart(id);
    	}
    }
	
	public void showPart(String id) {
		partmap.get(id).enabled = true;
	}
	
	public void animate(String id, boolean loop) {
		anim.animate(id, loop ? -1 : 1, null, 0.3f);
	}

}
