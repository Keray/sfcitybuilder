package sf.gdx.shaders;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.Attribute;

public class OverlayAttribute extends Attribute {
	
    public final static String alias = "overlay";
    public static final long Overlay = register(alias);
    
    public Texture[] textures;

	public OverlayAttribute(Texture[] texts) {
		super(Overlay);
		textures = texts;
	}
//	public OverlayAttribute(Texture tex) {
//		super(Overlay);
//		textures = new Texture[]{tex};
//	}

	@Override
	public Attribute copy() {
		return new OverlayAttribute(textures);
	}

	@Override
	protected boolean equals(Attribute that) {
		if(this==that)
			return true;
		if(that instanceof OverlayAttribute) {
			return false;//this.fogmap == ((FogMapAttribute) that).fogmap;
		}
		return false;
	}

}
