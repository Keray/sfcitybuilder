package sf.gdx.shaders;

import static gl.GL.*;
import gl.GL;
import sf.gdx.SFMain;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.Attribute;
import com.badlogic.gdx.graphics.g3d.Attributes;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.shaders.BaseShader;
import com.badlogic.gdx.graphics.g3d.shaders.DefaultShader;
import com.badlogic.gdx.graphics.g3d.utils.DefaultShaderProvider;
import com.badlogic.gdx.graphics.g3d.utils.RenderContext;

public class GDXShader extends DefaultShader{
	
	Texture fagtex; // temporal, lol
	
	public GDXShader(Renderable renderable, Config config, Texture fog) {
		super(renderable, config);
		
		this.fagtex = fog;
		
		register("u_fogmap", new Setter() {
			@Override
			public void set(BaseShader shader, int inputID, Renderable renderable,
					Attributes combinedAttributes) {
				
				
				if(fagtex == null)
					return;
				final int unit = shader.context.textureBinder.bind(fagtex);
				//final int unit = shader.context.textureBinder.bind(((FogMapAttribute)(combinedAttributes.get(FogMapAttribute.Fogmap))).getFogmap());
				shader.set(inputID, unit);
			}
			
			@Override
			public boolean isGlobal(BaseShader shader, int inputID) {
				return true;
			}
		});
		
		
		register("u_fogFlag", new Setter() {
			@Override
			public void set(BaseShader shader, int inputID, Renderable renderable,
					Attributes combinedAttributes) {
				//System.out.println("FOG TEX" + fagtex);
				shader.set(inputID, fagtex == null ? GL_FALSE : GL_TRUE);
			}
			
			@Override
			public boolean isGlobal(BaseShader shader, int inputID) {
				return true;
			}
		});
		

		register("u_overlays", new Setter() {
			int[] units = new int[10];
			@Override
			public void set(BaseShader shader, int inputID, Renderable renderable,
					Attributes combinedAttributes) {
				//Attribute attr = renderable.material.get(OverlayAttribute.Overlay);
				OverlayAttribute attribute = (OverlayAttribute) combinedAttributes.get(OverlayAttribute.Overlay);
				if(attribute == null || attribute.textures==null) {
					//System.out.println("doesnt have " + attr);
					shader.program.setUniformi("u_overnum", 0);
					return;
				}
				int overnum = attribute.textures.length;
				for(int i=0; i<overnum; ++i) {
					units[i] = shader.context.textureBinder.bind(attribute.textures[i]);
				}
				int location = shader.program.getUniformLocation("u_overlays");
				shader.program.setUniformi("u_overnum", overnum);
				GL.glUniform1iv(location, overnum, units);
				
				
//				Texture[] textures = attribute.textures;
//				int num = textures.length;
//				for(int i=0; i<num; i++) {
//					int location = shader.program.getUniformLocation("u_overlays");
//					units[0] = shader.context.textureBinder.bind(textures[i]);
//					System.out.println("lcation: " + location);
//					GL.glUniform1iv(location, 1, units);
//				}
//				shader.program.setUniformi("u_overnum", num);
//				System.out.println("num: " + num);
//				int location = shader.program.getUniformLocation("u_overlays");
//				
//				System.out.println("lcation: " + location);
//				GL.glUniform1iv(location, num, units);
				//shader.set(uniform, value)
			}
			
			@Override
			public boolean isGlobal(BaseShader shader, int inputID) {
				return false;
			}
		});
	}

	
	
}
