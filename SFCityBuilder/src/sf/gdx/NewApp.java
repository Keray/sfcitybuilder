package sf.gdx;

import static gl.GL.*;
import sf.gdx.ms3d.MS3DLoader;
import sf.gdx.ms3d.MS3DLoader.MS3DParameters;
import sf.gdx.shaders.GDXShader;

import com.badlogic.gdx.*;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g3d.*;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.model.Animation;
import com.badlogic.gdx.graphics.g3d.shaders.DefaultShader;
import com.badlogic.gdx.graphics.g3d.utils.DefaultShaderProvider;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

public class NewApp extends ApplicationAdapter {
	public OrthographicCamera cam;
	public RTSCameraControl rtscam;

	public Array<MS3DInstance> instances = new Array<>(MS3DInstance.class);
	private Environment env;
	private ModelBatch batch;
	
	private MS3DInstance controlled;
	
	private InputAdapter inp = new InputAdapter() {
		public boolean keyDown(int key) {
			if(key == Keys.SPACE) {
				Array<Animation> anims = controlled.mi.animations;
				controlled.animate(anims.get((int) (Math.random() * anims.size)).id, true);
				return true;
			}
			return false;
		};
	};

	@Override
    public void create() {
		
		env = new Environment();
		env.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.6f, 0.6f, 0.6f, 0.9f));
		env.add(new DirectionalLight().set(1f, 1f, 1f, -1f, -0.8f, -0.2f));
		
		DefaultShaderProvider provider = new DefaultShaderProvider() {
			@Override
			protected Shader createShader(Renderable renderable) {
				DefaultShader shad = new GDXShader(renderable, config, null);
				return shad;
			}
		};
		provider.config.numBones = 20;
		provider.config.fragmentShader = Gdx.files.internal("shaders/default.frag").readString();
		provider.config.vertexShader = Gdx.files.internal("shaders/default.vert").readString();
		batch = new ModelBatch(provider);

		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		cam = new OrthographicCamera(20, h/w * 20);
		
		cam.position.set(100f, 100f, 100f);
		cam.lookAt(0, 0, 0);
		cam.near = 0.1f;
		cam.far = 300f;
		cam.update();
		
		rtscam = new RTSCameraControl(cam);
		Gdx.input.setInputProcessor(new InputMultiplexer(inp , rtscam));
		
	    MS3DLoader loader = new MS3DLoader(new InternalFileHandleResolver());

		// copied from XML
		MS3DParameters params = new MS3DParameters();
		params.add("stand", 0, 0, 0.5f);
		params.add("move", 5, 21, 1.5f);
		params.add("fire", 26, 28, 1.5f);
		params.add("reach_down", 30, 34, 1.2f);
		params.add("strike", 36, 39, 1.5f);
		params.add("fall", 40, 46, 2.1f);
		params.add("look", 47, 48, 1.3f);
		params.add("talk", 49, 50, 2f);
		params.add("talk_long", 52, 57, 4f);
		params.add("evade", 58, 62, 1f);
		params.add("build", 63, 65, 1f);
		params.add("block", 66, 67, .5f);
		params.add("strike_big", 70, 74, 1.5f);
		params.add("move_fast", 75, 83, .7f);
		params.add("move_sneak", 85, 93, .7f);
		params.scale = 0.025f;
		
	    Model model = loader.loadModel(loader.resolve("xe/male_final.ms3d"), params);
	    
	    
	    Texture[] overlays = new Texture[3];
	    overlays[0] = new Texture("xe/physician_skin.gif");
	    overlays[1] = new Texture("xe/ecologist_skin.gif");
	    overlays[2] = new Texture("xe/highborn_male_skin.gif");
	    
	    {
		    MS3DInstance guy = new MS3DInstance(model);
		    guy.setOverlaySkins(overlays);
		    guy.hideParts("staff", "spear", "heavy blade");
		    guy.mi.transform.setToTranslation(0, 0, 0);
		    guy.mi.transform.setToRotation(Vector3.Y, 90);
		    instances.add(guy);
		    controlled = guy;
	    }
	    {
		    MS3DInstance guy = new MS3DInstance(model);
		    guy.mi.transform.setToTranslation(1, 0, 0);
		    instances.add(guy);
		    guy.animate("move", true);
	    }
	    
    }

	@Override
    public void resize(int width, int height) {
	    //cam.viewportWidth = width;
	    cam.viewportHeight = (float)height/width * 20;
    }

	@Override
    public void render() {
		

		rtscam.update();
		
//		if(controlled != null) {
//			controlled.mi.transform.setToTranslation(rtscam.target);
//		}
		
		float delta = Gdx.graphics.getDeltaTime();

		glClearColor(1, 0, 1, 0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		batch.begin(cam);
		for (MS3DInstance instance : instances) {
			instance.anim.update(delta);
			batch.render(instance.mi, env);
			//celbatch.render(instance, outline);
		}
		batch.end();
    }

}
