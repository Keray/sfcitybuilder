package sf.gdx;


import static gl.GL.*;
import sf.gdx.ms3d.*;
import sf.gdx.ms3d.MS3DLoader.MS3DParameters;
import sf.gdx.shaders.*;
import sf.gdx.ter.TerrainRender;

import com.badlogic.gdx.*;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g3d.*;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.model.NodePart;
import com.badlogic.gdx.graphics.g3d.shaders.DefaultShader;
import com.badlogic.gdx.graphics.g3d.utils.*;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.*;

public class SFMain implements ApplicationListener {
	public OrthographicCamera cam;
	public RTSCameraControl rtscam;
	public AssetManager assets;
	
	
	public Array<ModelInstance> instances = new Array<ModelInstance>();
	public boolean loading;
	private AnimationController ctrl1;
	private AnimationController ctrl4;
	
	private String model1 = "xe/male_final.ms3d";
	private String model4 = "micovore/Micovore.ms3d";
	
	
	
	private TerrainRender ter;
	
	ModelBatch celbatch;
//	ModelBatch blackbatch;
	public Texture fogtex;
	
	private Shader outline;
	
	Environment env;
	private Pixmap fogpix;
	private InputAdapter inp = new InputAdapter() {
		public boolean keyDown(int key) {
			if(key == Keys.SPACE) {
				String anim = ctrl1.target.animations.get((int) (Math.random() * ctrl1.target.animations.size)).id;
				
				ctrl1.animate(anim, -1, 1f, null, 1);
				return true;
			}
			return false;
		};
	};
	private Texture ph;

	@Override
	public void create() {
		
		

		fogpix = new Pixmap(128, 128, Format.RGBA8888);
		
		for(int x=0; x<fogpix.getWidth(); x++) {
			for(int z=0; z<fogpix.getHeight(); z++) {
				int dupa = (int) (Math.random() * 255);
				//System.out.println(dupa);
				if(z>8)
					dupa = 180;
				if(z>9)
					dupa = 255;
				fogpix.drawPixel(x, z, dupa);
			}
		}
		
		fogtex = new Texture(fogpix);
		fogtex.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		//fogtex.setWrap(TextureWrap.ClampToEdge, TextureWrap.ClampToEdge);
		
		fogtex = null;

		
		
		env = new Environment();
		env.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.6f, 0.6f, 0.6f, 0.9f));
		env.add(new DirectionalLight().set(1f, 1f, 1f, -1f, -0.8f, -0.2f));
		
		DefaultShaderProvider provider = new DefaultShaderProvider() {
			@Override
			protected Shader createShader(Renderable renderable) {
				DefaultShader shad = new GDXShader(renderable, config, fogtex);
				//System.out.println("----------------------------");
				//System.out.println(shad.program.getVertexShaderSource());
				//System.out.println("----------------------------");
				return shad;
			}
		};
		provider.config.numBones = 20;
		provider.config.fragmentShader = Gdx.files.internal("shaders/default.frag").readString();
		provider.config.vertexShader = Gdx.files.internal("shaders/default.vert").readString();
		celbatch = new ModelBatch(provider);
		
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		
		//cam = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		cam = new OrthographicCamera(20, h/w * 20);
		
		cam.position.set(100f, 100f, 100f);
		cam.lookAt(0, 0, 0);
		cam.near = 0.1f;
		cam.far = 300f;
		cam.update();
		
		rtscam = new RTSCameraControl(cam);
		Gdx.input.setInputProcessor(new InputMultiplexer(inp , rtscam));
		
		
		// copied straight from xml
		MS3DParameters params = new MS3DParameters();
		params.add("stand", 0, 0, 0.5f);
		params.add("move", 5, 21, 1.5f);
		params.add("fire", 26, 28, 1.5f);
		params.add("reach_down", 30, 34, 1.2f);
		params.add("strike", 36, 39, 1.5f);
		params.add("fall", 40, 46, 2.1f);
		params.add("look", 47, 48, 1.3f);
		params.add("talk", 49, 50, 2f);
		params.add("talk_long", 52, 57, 4f);
		params.add("evade", 58, 62, 1f);
		params.add("build", 63, 65, 1f);
		params.add("block", 66, 67, .5f);
		params.add("strike_big", 70, 74, 1.5f);
		params.add("move_fast", 75, 83, .7f);
		params.add("move_sneak", 85, 93, .7f);

		assets = new AssetManager();
		assets.setLoader(Model.class, ".ms3d", new MS3DLoader(new InternalFileHandleResolver()));
		
		assets.load(model1, Model.class, params);
		//assets.load(model4, Model.class);
		loading = true;
		
		//shad = new BlackShader();
		
		ter = new TerrainRender(cam);
		
		
		ph = new Texture("xe/physician_skin.gif");
		
		
		
		outline = new OutlineShader();
		outline.init();
	}
	
	MS3DInstance ludzik;

	private void doneLoading() {

		if(assets.isLoaded(model1)) {
			
			Model kutas = assets.get(model1, Model.class);
			kutas.nodes.get(0).scale.scl(0.1f, 0.1f, 0.1f);
			kutas.nodes.get(0).globalTransform.scale(0.5f, 0.5f, 0.5f);
			kutas.nodes.get(0).parts.get(0).bones = new Matrix4[0];
			Material mat = kutas.materials.get(0);
			//BlendingAttribute bl = (BlendingAttribute) mat.get(BlendingAttribute.Type);
			//System.out.println("OPACITY: " + bl.opacity);
			//bl.opacity = 1f;
			//bl.blended = false;
			

//		    <animation name="stand"      start="0"  end="0"  duration="0.5" />
//		    <animation name="move"       start="5"  end="21" duration="1.5" />
//		    <animation name="fire"       start="26" end="28" duration="1.5" />
//		    <animation name="reach_down" start="30" end="34" duration="1.2" />
//		    <animation name="strike"     start="36" end="39" duration="1.5" />
//		    <animation name="fall"       start="40" end="46" duration="2.1" />
//		    <animation name="look"       start="47" end="48" duration="1.3" />
//		    <animation name="talk"       start="49" end="50" duration="2"   />
//		    <animation name="talk_long"  start="52" end="57" duration="4"   />
//		    <animation name="evade"      start="58" end="62" duration="1"   />
//		    <animation name="build"      start="63" end="65" duration="1"   />
//		    <animation name="block"      start="66" end="67" duration="0.5" />
//		    <animation name="strike_big" start="70" end="74" duration="1.5" />
//		    <animation name="move_fast"  start="75" end="83" duration="0.7" />
//		    <animation name="move_sneak" start="85" end="93" duration="0.7" />

			

			String[] ded = {"staff", "pistol", "spear", "heavy blade", "sleeves", "Skirt"};
			ObjectSet<String> deactivated = new ObjectSet<>();
			deactivated.addAll(ded);
			
			{
				ludzik = new MS3DInstance(kutas);
				instances.add(ludzik.mi);
				for(String gr : ded) {
					ludzik.hidePart(gr);
				}
				ludzik.setOverlaySkins(ph);
				//ludzik.mi.nodes.get(0).parts.get(0).m
				System.out.println("MATERIALS NUM: " + ludzik.mi.materials.size);
//				for(Material m : ludzik.mi.materials) {
//					m.set(new OverlayAttribute(new Texture[] {ph}));
//				}
				//ludzik.mi.transform.translate(x, y, z)
				//ModelInstance k = new ModelInstance(kutas);
				//k.transform.idt();
				//k.transform.translate(1, 1, 2);
				ctrl1 = ludzik.anim;
				ctrl1.animate("move", -1, 1f, null, 1);
			}
			{
				ModelInstance k = new ModelInstance(kutas);
				k.transform.translate(1, 0, 0);
				instances.add(k);
			}
			
			
		}

		if(assets.isLoaded(model4)) {
			Model kutas = assets.get(model4, Model.class);
			
			ModelInstance k = new ModelInstance(kutas);
			k.transform.translate(0, 0, 10);
			System.out.println("Model nodes: " + k.nodes.get(0).parts.removeIndex(0));
			//k.transform.scale(0.1f, 0.1f, 0.1f);
			
			instances.add(k);

			ctrl4 = new AnimationController(k);
			ctrl4.animate(k.animations.get(0).id, -1, 0.2f, null, 1);
		}

		loading = false;
		//Gdx.graphics.setVSync(true);
	}

	@Override
	public void render() {
		if (loading && assets.update())
			doneLoading();
		rtscam.update();
		
		
		if(ludzik != null) {
			ludzik.mi.transform.setToTranslation(rtscam.target);
		}
		
		glClearColor(1, 0, 0, 0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		if (ctrl1 != null) {
			ctrl1.update(Gdx.graphics.getDeltaTime());
		}
		if (ctrl4 != null) {
			ctrl4.update(Gdx.graphics.getDeltaTime());
		}
		
		

		if(fogtex != null)
			fogtex.draw(fogpix, 0, 0);
		
		ter.render(fogtex);
		
		celbatch.begin(cam);
		for (ModelInstance instance : instances) {
			celbatch.render(instance, env);
			//celbatch.render(instance, outline);
		}
		celbatch.end();

		
		// rendering outlines
		
//		celbatch.begin(cam);
//		for (ModelInstance instance : instances) {
//			celbatch.render(instance, outline);
//		}
//		celbatch.end();
		
		if(Gdx.graphics.getDeltaTime() > 0.03)
			System.out.println("LONG FRAME");
		
		
		// from pixmap to texture
		// you can change fog pixmap here
		// and then send it to texture
		
	}

	@Override
	public void dispose() {
		celbatch.dispose();
		instances.clear();
		assets.dispose();
	}

	public void resume() {
	}

	public void resize(int width, int height) {
		cam.viewportWidth = 20;
		cam.viewportHeight = (float)height/width * 20;
		cam.update();
	}

	public void pause() {
	}
}
