package sf.gdx.ter;

import static gl.GL.*;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.glutils.*;
import com.badlogic.gdx.utils.*;

public class TerrainRender {

	ShaderProgram tshader;
	
	TerrainRenderable chunk;
	
	Camera camera;

	private Texture texture;
	
	public TerrainRender(Camera cam) {
		tshader = new ShaderProgram(Gdx.files.internal("shaders/terrain.vert"), Gdx.files.internal("shaders/terrain.frag"));
		
		
		TerTileLoad load = new TerTileLoad();
		
		camera = cam;
		chunk = new TerrainRenderable(16, 16);
		chunk.mesh.setAutoBind(true);
		chunk.types = load.load();
		
		load.update();
		texture = load.texture;
//		texture = new Texture(Gdx.files.internal("tiles/lol.png"));//.texture;
		
		
		

		chunk.tiledata[3][3] = 1;
		chunk.tiledata[3][4] = 1;
		chunk.tiledata[3][5] = 2;
		
		
		//TileType[] types = {new TileType(tex, 0, 0), new TileType(tex, 1, 0), new TileType(tex, 0, 1), new TileType(tex, 1, 1), new TileType(tex, 5, 1)};
		System.out.println("generating mesh");
		chunk.generateMesh();
		System.out.println("generated lol");
		
		if(!tshader.isCompiled()) {
			throw new GdxRuntimeException(tshader.getLog());
		}
	}
	
	public void render(Texture fog) {
		
		//Gdx.gl.glEnable(GL10.GL_CULL_FACE);
		Gdx.gl.glEnable(GL10.GL_DEPTH_TEST);
		Gdx.gl.glDepthFunc(GL20.GL_LESS);
		Gdx.gl.glEnable(GL10.GL_BLEND);
		Gdx.gl.glDepthMask(true);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
	
		//tshader.pedantic = false;
		
		texture.bind(0);
		if(fog!=null)
			fog.bind(1);
		
		tshader.begin();
		
		tshader.setUniformi("u_texture", 0);
		tshader.setUniformi("u_fog", 1);
		
		tshader.pedantic = true;
		tshader.setUniformi("u_fogFlag", fog == null ? GL_FALSE : GL_TRUE );
		
		
		tshader.setUniformMatrix("u_projTrans", camera.combined);
		
		chunk.mesh.render(tshader, GL20.GL_TRIANGLES);
		
		tshader.end();
	}

}
